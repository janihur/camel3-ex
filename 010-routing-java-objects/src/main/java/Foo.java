public class Foo {
    Foo(int id) {
        this.fooId = id;
    }

    public int getFooId() {
        return fooId;
    }

    private int fooId = 0;
}
